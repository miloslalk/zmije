import { Component, OnInit } from '@angular/core';
import {ZmijeModel} from '../../model/zmije.model';
import { config } from '../../config';

@Component({
  selector: 'table-zmija',
  templateUrl: './table-zmija.route.html'
})
export class TableZmijaComponent implements OnInit {

  conf = config.zmijeTableConfig;

  constructor(public model:ZmijeModel) { }

  ngOnInit() {
  }

}
