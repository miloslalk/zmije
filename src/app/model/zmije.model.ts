import { Injectable } from '@angular/core';
import { ZmijeService } from '../services/zmije.services';

@Injectable()
export class ZmijeModel{

    zmije = [];

    constructor(private service:ZmijeService){
        let posmatracSvihZmija = this.service.vratiPosmatracSvihZmijaNaServeru();
        posmatracSvihZmija.subscribe(
            (zmije)=>{
                this.zmije = zmije;
            }
        );
    }

}