import { Injectable } from '@angular/core';
import {Router} from '@angular/router'

@Injectable()
export class MenuModel {

  constructor(private router:Router) {}

  linkovi=[
    {putanja:"/", naziv:"Kuća"},
    {putanja:"dashboard", naziv:"Tabela zmija"},
    {putanja:"zmije/novi", naziv:"Dodaj zmiju"}
  ]
  goToPath(path){
    let pathArray = [];
    pathArray.push(path);
    this.router.navigate(pathArray);
  }
}
