import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';

import { ZmijeComponent } from './zmije.component';
import { HeaderComponent } from './components/header/header';
import { MenuComponent } from './components/menu/menu';
import { SidebarComponent } from './components/sidebar/sidebar';
import { FooterComponent } from './components/footer/footer';

import { MenuModel } from './model/menu.model';

import { HomeComponent } from './routes/home/home.route';
import { TableZmijaComponent } from './routes/table-zmija/table-zmija.route';
import { NovaZmijaComponent } from './routes/nova-zmija/nova-zmija.route';
import { PingvinComponent } from './routes/pingvin/pingvin.route';
import { EditujPingvinaComponent } from './routes/edituj-pingvina/edituj-pingvina.route';
import { DetaljiPingvinaComponent } from './routes/detalji-pingvina/detalji-pingvina.route';
import { MenuLinkComponent } from './components/menu-link/menu-link.component';
import { PingviniModel } from './models/pingvini.model';
import { PingviniService } from './services/pingvini.service';
import { DetaljiZmijaComponent } from './routes/detalji-zmija/detalji-zmija.component';
import { EditujZmijuComponent } from './routes/edituj-zmiju/edituj-zmiju.component';
import { HomeRouteComponent } from './routes/home/home-route/home-route.component';
import { NovaZmijaComponent } from './routes/nova-zmija/nova-zmija.component';
import { ZmijaComponent } from './routes/zmija/zmija.component';
import { TableZmijaComponent } from './routes/table-zmija/table-zmija.route';


const routes:Routes = [
  {path:"",component:HomeComponent},
  {path:"dashboard",component:TablePingvinaComponent},
  {path:"pingvini/novi",component:NoviPingvinComponent},
  {path:"pingvin/:id",component:PingvinComponent},
  {path:"pingvin/:id/edit/",component:EditujPingvinaComponent},
  {path:"pingvin/:id/info/",component:DetaljiPingvinaComponent},
]

@NgModule({
  declarations: [
    ZmijeComponent,
    HeaderComponent,
    MenuComponent,
    SidebarComponent,
    FooterComponent,
    HomeComponent,
    TablePingvinaComponent,
    NoviPingvinComponent,
    PingvinComponent,
    EditujPingvinaComponent,
    DetaljiPingvinaComponent,
    MenuLinkComponent,
    DetaljiZmijaComponent,
    EditujZmijuComponent,
    HomeRouteComponent,
    NovaZmijaComponent,
    ZmijaComponent,
    TableZmijaComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    MenuModel,
    PingviniModel,
    PingviniService
  ],
  bootstrap: [ZmijeComponent]
})
export class PingviniModule { }
