import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {config} from '../config';
import "rxjs/add/operator/map";

@Injectable()
export class  ZmijeService{

    constructor(private http:Http){ }

    vratiPosmatracSvihZmijaNaServeru(){
        return this.http
            .get(config.apiUrl + '/zmije')
            .map((response)=>{return response.json()});
    }
}